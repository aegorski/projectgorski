# Electronic Portfolio 

On this site you will find the work of Alecia Gorski. Alecia Gorski is a Program Advisor and Technology Specialist working at Lorain County Community College in Elyria, Oh. 

![Black and White](https://gitlab.com/aegorski/projectgorski/-/raw/main/docs/Images/index/blackandwhite.JPEG)


## Career
<details>
<summary>Ohio State University</summary>
</p>
 - 1st Semester Lecturer 
</p>
 - Manufacturing Processes II 
</p>

</details>
</p>

<details>
<summary>Lorain County Community College</summary>
</p>
 - 4 Years experience working multiple departments. 
</p>
 - Program Advisor, Particioant Advisor
</p>
 - Coordinate multi-departmental efforts to ensure success of events aligned to federal grants
</p>
 - Design and facilitate community programs
</p>
 - Create marketing collateral including: webpage designs, flyers, stand up advertisement and career pathway graphics; maintain Ohio TechNet social media accounts; utilize web-based tools including: Canvas, Canva, Smartsheet, ect. 
</p>
 - Track, communicate and intercact with grant participants in multiple cohorts
</p>
 - Recruit at community and partinership events  

</details>
</p>

## Education

<details><summary>MIT - Fab Academy</summary>
</p>
 - 2020 Graduate of How to Make "Almost" Anything 
</p>
 - MIT Certified to run a fabrication Lab
</p>
 - Solar Powered Leaf Light (https://aegorski.gitlab.io/projectgorski/Life/FabAcademy/)
</p>
</details>
</p>
<details><summary>Ball State University</summary>
</p>
 - Masters Degree in Technology Education  
</p>
 - Bachelors Degree in Print Based Media
</p>
</details>
</p>

## Hobbies

<details><summary>Bird Nerd</summary>
</p>
 - Can name over 220 bird with no reference  
</p>
 - 4 Years of citizen ornithalogical studies 
</p>
</details>
</p>
<details><summary>Nature Photographer</summary>
</p>
 - Nikon D3400 with a 70mm-300mm Zoom Lens 
</p>
 - Photo confirmed almost 200 bird species 
</p>
</details>
</p>
<details><summary>Avid Mountain Climber</summary>
</p>
 - 10 mountains climbed to date  
</p>
 - Elevations reaching 1530ft 
</p>
</details>
</p>
<details><summary>Contiually Studying Electronics</summary>
</p>
 - Code my own site
</p>
 - Practice coding regularly, researching to solve my own issues
</p>
</details>
</p>
<details><summary>Avid Reader</summary>
</p>
 - Birds & Nature
</p>
 - Biographies
</p>
 - Stephen King
</p>
</details>
</div>