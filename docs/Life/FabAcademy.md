# Fab Academy 

## What is Fab Academy?
### Learn to Make (Almost) Anything

The Fab Academy is a fast paced, hands-on learning experience where students learn rapid-prototyping by planning and executing a new project each week, resulting in a personal portfolio of technical accomplishments.

## My Project

![Presentation Slide](https://gitlab.com/aegorski/projectgorski/-/raw/main/docs/Life/images/presentation.PNG)

My goal in Fab Academy was to better understand the technology surrounding us. I wanted to design a project I knew nothing about and learn each step. The Solar Powered Leaf Light became my idea. Routing the body of the light would be done using upcycled wood donated from surrounding business scrap. The PETG would hopefully be created using upcycled materials as well. The light will be set to a solar powered circuit, but rechargeable batteries are currently being used. The RGB LED is connected to the phototransistor and fed into the ESP32. The ESP32 (microcontroller) can also connect with WiFi to an app that controls each RGB color separately. The input (phototransistor readings) controls when the output (RGB LED pins) is activated.

<iframe width="808" height="455" src="https://www.youtube.com/embed/j_e1HkaTzXw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Project Video](http://fabacademy.org/2020/labs/lccc/students/alecia-gorski/presentation.mp4)


<div class="card text-white bg-primary mb-3" style="max-width: 50rem;">
  <div class="card-header">Alecia Gorski</div>
  <div class="card-body">
    <h4 class="card-title">2020 Fab Academy Graduate</h4>
    <p class="card-text"> 
    	<a href="https://fabacademy.org/2020/labs/lccc/students/alecia-gorski/">2020 Fab Academy Documentation</a>
    	of final project. Solar Powered Leaf Light and educational table and 3D printed charging stand. </p>
  </div>
</div>
